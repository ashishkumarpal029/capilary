package com.ashish.capilary.di.builder;

import com.ashish.capilary.view.fragment.MoviesListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuilderModule {

    @SuppressWarnings("unused")
    @ContributesAndroidInjector
    abstract MoviesListFragment contributeMovieListFragment();
}