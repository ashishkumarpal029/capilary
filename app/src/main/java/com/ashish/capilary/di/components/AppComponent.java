package com.ashish.capilary.di.components;

import android.app.Application;

import com.ashish.capilary.BaseApplication;
import com.ashish.capilary.di.builder.ActivityBuilderModule;
import com.ashish.capilary.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AppModule.class, AndroidInjectionModule.class, ActivityBuilderModule.class})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(BaseApplication baseApplication);
}
