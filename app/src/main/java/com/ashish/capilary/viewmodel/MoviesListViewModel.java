package com.ashish.capilary.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.ashish.capilary.data.local.entity.MoviesEntity;
import com.ashish.capilary.data.remote.ApiConstants;
import com.ashish.capilary.data.remote.Resource;
import com.ashish.capilary.data.remote.model.MoviesCastList;
import com.ashish.capilary.data.remote.repository.MoviesRepository;

import java.util.List;

import javax.inject.Inject;

public class MoviesListViewModel extends ViewModel {
    private final LiveData<Resource<List<MoviesEntity>>> nowPlayingMovies;
    MoviesRepository repository;

    @Inject
    public MoviesListViewModel(MoviesRepository moviesRepository) {
        repository = moviesRepository;
        nowPlayingMovies = repository.loadNowPlayingMovies(ApiConstants.API_KEY, 1);
    }

    public LiveData<Resource<MoviesCastList>> movieCastApi(long movieId) {
       return repository.getCastList(movieId, ApiConstants.API_KEY);

    }

    public LiveData<Resource<List<MoviesEntity>>> getNowPlayingMovies() {
        return nowPlayingMovies;
    }
}
