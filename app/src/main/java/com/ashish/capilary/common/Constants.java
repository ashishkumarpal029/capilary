package com.ashish.capilary.common;

public class Constants {
    public static final String BUNDLE_KEY_IMAGE_URL = "image_url";
    public static final String BUNDLE_KEY_TITILE = "title";
    public static final String BUNDLE_KEY_RATING = "rating";
    public static final String BUNDLE_KEY_OVERVIEW = "overview";
}
