package com.ashish.capilary.data.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ashish.capilary.data.local.entity.MoviesEntity;

import java.util.List;

@Dao
public interface MoviesDao {
    @Query("SELECT * FROM movies")
    LiveData<List<MoviesEntity>> loadNowPlayingMovies();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveMovies(List<MoviesEntity> moviesEntities);
}
