package com.ashish.capilary.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.ashish.capilary.data.local.dao.MoviesDao;
import com.ashish.capilary.data.local.entity.MoviesEntity;

@Database(entities = {MoviesEntity.class}, version = 2, exportSchema = false)
public abstract class MoviesDatabase extends RoomDatabase {
    public abstract MoviesDao moviesDao();
}