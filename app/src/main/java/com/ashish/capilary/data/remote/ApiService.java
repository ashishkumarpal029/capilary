package com.ashish.capilary.data.remote;

import com.ashish.capilary.data.remote.model.MoviesCastList;
import com.ashish.capilary.data.remote.model.MoviesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("now_playing?")
    Call<MoviesList> getNowPlaying(@Query("api_key") String api_key,@Query("page") long page);

    @GET("{movie_id}/credits?")
    Call<MoviesCastList> getCast(@Path("movie_id") String  movie_id, @Query("api_key") String api_key);
}
