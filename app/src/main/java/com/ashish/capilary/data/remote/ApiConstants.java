package com.ashish.capilary.data.remote;

public class ApiConstants {
    public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";
    public static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w138_and_h175_face";
    public static final long CONNECT_TIMEOUT = 30000;
    public static final long READ_TIMEOUT = 30000;
    public static final long WRITE_TIMEOUT = 30000;
    public static final String API_KEY = "da7d633e8c9499395e3e15f427778e1a";
}