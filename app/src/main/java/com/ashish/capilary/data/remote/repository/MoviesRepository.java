package com.ashish.capilary.data.remote.repository;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.ashish.capilary.data.local.dao.MoviesDao;
import com.ashish.capilary.data.local.entity.MoviesEntity;
import com.ashish.capilary.data.remote.ApiService;
import com.ashish.capilary.data.remote.NetworkBoundResource;
import com.ashish.capilary.data.remote.Resource;
import com.ashish.capilary.data.remote.model.MoviesCastList;
import com.ashish.capilary.data.remote.model.MoviesList;
import com.ashish.capilary.view.callbacks.ResponseListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesRepository {

    private final MoviesDao moviesDao;
    private final ApiService apiService;

    @Inject
    MoviesRepository(MoviesDao moviesDao, ApiService apiService) {
        this.moviesDao = moviesDao;
        this.apiService = apiService;
    }

    /**
     * This method fetches the now playing movies from the service.
     * Once the fetching is done the data is cached to local db so that the app can even work offline
     *
     * @param apiKey index indicating the api key for TMDB
     * @param page   index indicating the current page
     * @return List of movies
     */
    public LiveData<Resource<List<MoviesEntity>>> loadNowPlayingMovies(final String apiKey, final int page) {
        return new NetworkBoundResource<List<MoviesEntity>, MoviesList>() {
            @Override
            protected void saveCallResult(MoviesList item) {
                if (null != item)
                    moviesDao.saveMovies(item.getResults());
            }

            @NonNull
            @Override
            protected LiveData<List<MoviesEntity>> loadFromDb() {
                return moviesDao.loadNowPlayingMovies();
            }

            @NonNull
            @Override
            protected Call<MoviesList> createCall() {
                return apiService.getNowPlaying(apiKey, page);
            }
        }.getAsLiveData();
    }

    //from here we have to start ----
    public LiveData<Resource<MoviesCastList>> getCastList(long movieID, final String apiKey) {
        MutableLiveData<Resource<MoviesCastList>> data = new MediatorLiveData<>();
        Call<MoviesCastList> call = apiService.getCast(String.valueOf(movieID), apiKey);
        call.enqueue(new Callback<MoviesCastList>() {
            @Override
            public void onResponse(Call<MoviesCastList> call, Response<MoviesCastList> response) {
                data.postValue(Resource.success(response.body()));
                System.out.println("sdasdasd" + response.body());
            }

            @Override
            public void onFailure(Call<MoviesCastList> call, Throwable t) {
                System.out.println("sdasdasd" + t.toString());
            }
        });
        return data;
    }

    /**
     * This method fetches the HTML comntent from the url and parses it and fills the model
     *
     * @param url              url to be fetched
     * @param responseListener callback
     */
    @SuppressLint("CheckResult")
    public void loadMoviesDetails(final String url, final ResponseListener responseListener) {
        final MoviesEntity movieDetails = new MoviesEntity();
        Observable.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Document document = Jsoup.connect(url).get();
                movieDetails.setTitle(document.title());
                //movieDetails.setPosterPath(document.);
                return false;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                               @Override
                               public void accept(Object result) throws Exception {
                                   responseListener.onSuccess(movieDetails);
                               }
                           },
                        (new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable error) throws Exception {
                                responseListener.onFailure(error.getMessage());
                            }
                        }));

    }

}
