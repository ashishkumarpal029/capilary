package com.ashish.capilary.data.remote;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
