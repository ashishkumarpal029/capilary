package com.ashish.capilary.view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.ashish.capilary.R;
import com.ashish.capilary.databinding.ActivityMainBinding;
import com.ashish.capilary.utils.FragmentUtils;
import com.ashish.capilary.view.base.BaseActivity;
import com.ashish.capilary.view.fragment.MoviesListFragment;

import static com.ashish.capilary.utils.FragmentUtils.TRANSITION_NONE;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentUtils.replaceFragment(this, MoviesListFragment.newInstance(), R.id.mainContainer, false, TRANSITION_NONE);
    }
}
