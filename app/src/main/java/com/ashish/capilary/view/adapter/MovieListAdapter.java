package com.ashish.capilary.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ashish.capilary.R;
import com.ashish.capilary.data.local.entity.MoviesEntity;
import com.ashish.capilary.data.remote.model.MoviesCastList;
import com.ashish.capilary.view.base.BaseAdapter;
import com.ashish.capilary.view.callbacks.MoviesListCallback;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class MovieListAdapter extends BaseAdapter<MovieListAdapter.MovieViewHolder, MoviesEntity> {
    private List<MoviesEntity> movieEntities;
    private final MoviesListCallback moviesListCallback;
    private List<MoviesCastList.Cast> castList;
    private int enableValue = -1;
    private Context context;
    private int selectedPosition = -1;

    public MovieListAdapter(@NonNull MoviesListCallback moviesListCallback, Context context) {
        movieEntities = new ArrayList<>();
        castList = new ArrayList<>();
        this.moviesListCallback = moviesListCallback;
        this.context = context;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_movie, parent, false);
        return new MovieViewHolder(view, moviesListCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.bind(movieEntities.get(position));


        CastAdapter castAdapter = new CastAdapter(castList, context);
        castAdapter.setData(castList);
        holder.recyclerView.setAdapter(castAdapter);
        if (selectedPosition == position) {
            holder.llCast.animate().translationY(70);
            holder.llCast.setVisibility(View.VISIBLE);
        } else {
            holder.llCast.setVisibility(View.GONE);

        }

        holder.itemView.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moviesListCallback.onMovieClicked(movieEntities.get(position));
                selectedPosition=position;
            }
        });

    }

    @Override
    public int getItemCount() {
        return movieEntities.size();
    }

    @Override
    public void setData(List<MoviesEntity> movieEntities) {
        this.movieEntities = movieEntities;
        notifyDataSetChanged();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvRating, tvOverView;
        private ImageView ivThumbnail;
        RecyclerView recyclerView;
        LinearLayout llCast;

        private MovieViewHolder(View itemView, final MoviesListCallback callback) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvRating = itemView.findViewById(R.id.tvRating);
            tvOverView = itemView.findViewById(R.id.tvOverView);
            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            recyclerView = itemView.findViewById(R.id.rvCast);
            llCast = itemView.findViewById(R.id.llCast);


        }

        void bind(MoviesEntity moviesEntity) {
            tvTitle.setText(moviesEntity.getTitle());
            tvRating.setText(String.valueOf(moviesEntity.getVoteAverage()));
            tvOverView.setText(moviesEntity.getOverview());

            Glide.with(itemView.getContext())
                    .load(moviesEntity.getPosterPath())
                    .placeholder(R.drawable.place_holder)
                    .into(ivThumbnail);
        }
    }

    public void movieCastData(List<MoviesCastList.Cast> moviesCastList, int enableValue) {
        this.castList = moviesCastList;
        this.enableValue = enableValue;
        notifyDataSetChanged();
    }
}