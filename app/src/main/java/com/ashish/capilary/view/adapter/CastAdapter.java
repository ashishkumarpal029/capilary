package com.ashish.capilary.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ashish.capilary.R;
import com.ashish.capilary.data.remote.model.MoviesCastList;
import com.ashish.capilary.view.base.BaseAdapter;
import com.bumptech.glide.Glide;

import java.util.List;

public class CastAdapter extends BaseAdapter<CastAdapter.CastViewHolder, MoviesCastList.Cast> {
    List<MoviesCastList.Cast> movieCastList;
    Context context;

    public CastAdapter(@NonNull List<MoviesCastList.Cast> movieCastList, Context context) {
        this.movieCastList = movieCastList;
        this.context = context;
    }

    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_cast, parent, false);
        return new CastViewHolder(view, movieCastList);
    }

    @Override
    public void onBindViewHolder(@NonNull CastViewHolder holder, int position) {
        if(movieCastList.get(position).getName()!=null) {
            holder.actorName.setText(movieCastList.get(position).getName());
        }else {
            holder.actorName.setText("N/A");
        }

       Glide.with(context)
                .load(movieCastList.get(position).getProfilePath())
                .placeholder(R.drawable.place_holder)
                .into(holder.actorImage);
    }

    @Override
    public int getItemCount() {
        return movieCastList.size();
    }

    @Override
    public void setData(List<MoviesCastList.Cast> data) {
        this.movieCastList = data;
    }

    public class CastViewHolder extends RecyclerView.ViewHolder {
        private TextView actorName;
        private ImageView actorImage;

        private CastViewHolder(View itemView, final List<MoviesCastList.Cast> callback) {
            super(itemView);
            actorName = itemView.findViewById(R.id.actorName);
            actorImage = itemView.findViewById(R.id.actorImage);
        }
    }
}