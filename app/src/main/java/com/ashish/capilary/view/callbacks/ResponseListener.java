package com.ashish.capilary.view.callbacks;

import com.ashish.capilary.data.local.entity.MoviesEntity;

public interface ResponseListener {
    void onSuccess(MoviesEntity data);
    void onFailure(String message);
}
