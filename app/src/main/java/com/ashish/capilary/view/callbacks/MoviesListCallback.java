package com.ashish.capilary.view.callbacks;

import com.ashish.capilary.data.local.entity.MoviesEntity;




public interface MoviesListCallback {
    void onMovieClicked(MoviesEntity moviesEntity);
}
