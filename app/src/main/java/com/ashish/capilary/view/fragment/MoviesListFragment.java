package com.ashish.capilary.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ashish.capilary.R;
import com.ashish.capilary.data.local.entity.MoviesEntity;
import com.ashish.capilary.data.remote.Resource;
import com.ashish.capilary.data.remote.Status;
import com.ashish.capilary.data.remote.model.MoviesCastList;
import com.ashish.capilary.databinding.FragmentMovieListBinding;
import com.ashish.capilary.view.adapter.MovieListAdapter;
import com.ashish.capilary.view.base.BaseFragment;
import com.ashish.capilary.view.callbacks.MoviesListCallback;
import com.ashish.capilary.viewmodel.MoviesListViewModel;

import java.util.List;

public class MoviesListFragment extends BaseFragment<MoviesListViewModel, FragmentMovieListBinding> implements MoviesListCallback {
    MovieListAdapter movieListAdapter;

    public static MoviesListFragment newInstance() {
        Bundle args = new Bundle();
        MoviesListFragment fragment = new MoviesListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Class<MoviesListViewModel> getViewModel() {
        return MoviesListViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_movie_list;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        dataBinding.rvMoviesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        movieListAdapter = new MovieListAdapter(this,getActivity());
        dataBinding.rvMoviesList.setAdapter(movieListAdapter);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel.getNowPlayingMovies()
                .observe(this, new Observer<Resource<List<MoviesEntity>>>() {
                    @Override
                    public void onChanged(Resource<List<MoviesEntity>> listResource) {
                        if (null != listResource && (listResource.status == Status.ERROR || listResource.status == Status.SUCCESS)) {
                            dataBinding.pbLoading.setVisibility(View.GONE);
                        }
                        dataBinding.setResource(listResource);

                        // If the cached data is already showing then no need to show the error
                        if (null != dataBinding.rvMoviesList.getAdapter() && dataBinding.rvMoviesList.getAdapter().getItemCount() > 0) {
                            dataBinding.tvError.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    public void onMovieClicked(MoviesEntity moviesEntity) {
        if (null != getActivity()) {
            viewModel.movieCastApi(moviesEntity.getId()).observe(this, new Observer<Resource<MoviesCastList>>() {
                @Override
                public void onChanged(Resource<MoviesCastList> moviesCastListResource) {
                    movieListAdapter.movieCastData(moviesCastListResource.data.getCast(),1);
                 }
            });
        }
    }
}
